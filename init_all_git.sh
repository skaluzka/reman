#!/bin/bash
#set -v
set -e
#set -x

USER_NAME="skaluzka"
USER_EMAIL="$USER_NAME@riseup.net"


ROOT_DIR=$(pwd)
DIRS=$(grep --color dest_dir repos.xml | sed 's/<dest_dir>\(.*\)<\/dest_dir>/\1/g')
for d in $DIRS; do
    echo -e "\nswitching to repository dir: $d"
    cd $d
    echo "setting local config for repository"
    git config --local user.name $USER_NAME
    git config --local user.email $USER_EMAIL
    echo "local settings:"
    git config --local -l
    cd $ROOT_DIR
done
echo "done"




#70D0:

